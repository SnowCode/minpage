import os
from jinja2 import FileSystemLoader, Environment
from shutil import copyfile
from modules import inject, gett, mdToHtml, addToKey

# Define what is an article
class Article(object):
    def __init__(self, title, body, category, author, date):
        self.title = title
        self.body = body
        self.category = category
        self.author = author
        self.date = date

# Define the key lists to organize the articles
articles, categories, authors = [], {}, {}

# Modify the following if you want to treat some filetypes in another way
for f in os.listdir("content"):
    if f.endswith(".md"):
        # Convert the file
        mdToHtml(f"content/{f}", f"content/{f.split('.md')[0] + '.html'}")
        f = f.split('.md')[0] + '.html'

        # Load the converted file
        fa = open(f"content/{f}", "r").read()
        print(f"The content of '{f}' loaded.")

        # Get the info, modify this accordingly to the definition of your article:
        title = gett(fa, "{Title: ", "}")
        category = gett(fa, "{Category: ", "}")
        date = gett(fa, "{Date: ", "}")
        author = gett(fa, "{Author: ", "}")
        body = fa.split("{Body}")[1]

        # Now create the article (modify the list accordingly with the definition of your article)
        article = Article(title, body, category, author, date)
        articles.append(article)

        # Add the article into the right keys, follow the same paterns to add new keys
        categories = addToKey(categories, category, article)
        authors = addToKey(authors, author, article)

    # if any other type of files:
    else:
        print(f"File '{f}' has been moved.")
        copyfile(f"content/{f}", f"output/{f}")

# Create the templates
inject(articles, "index.html", "output/index.html")

# Create the lists and the keys
for article in articles:
    inject(article, "article.html", f"output/{article.title}.html")
for category in categories:
    data = [category, categories[category]]
    inject(data, "category.html", f"output/{category}.html")
for author in authors:
    data = [author, authors[author]]
    inject(data, "author.html", f"output/{author}.html")

print("Your generation is successful.")